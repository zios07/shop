package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import io.mockk.every
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.time.ZonedDateTime

@WebMvcTest
class ProductControllerTest @Autowired constructor(private val mockMvc: MockMvc, private val objectMapper: ObjectMapper) {

    @MockkBean
    lateinit var productService: ProductService

    val productsResponse = listOf(
        ProductEntity(
            "1", "product1", "product 1", BigDecimal.ONE, 100,
            ZonedDateTime.now(), ZonedDateTime.now()
        ).toProductResponse(),
        ProductEntity(
            "2", "product2", "product 2", BigDecimal.ONE, 200,
            ZonedDateTime.now(), ZonedDateTime.now()
        ).toProductResponse()
    )

    val productRequest = ProductRequest(
        "123", "product1", "product 1", BigDecimal.ONE, 100
    )

    @Test
    fun `Given existing product, when GET request, return product with 200 status`() {
        every { productService.findProductsBySkus(listOf("1", "2", "3")) } returns productsResponse

        mockMvc.perform(get("/products/1,2,3"))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize<List<ProductResponse>>(2)))
    }

    @Test
    fun `Given a product, when a PUT request to edit its properties, return product with edited values and 200 status`() {
        every { productService.updateProduct(productRequest) } returns productsResponse[0]

        mockMvc.perform(
            put("/products")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(productRequest))
        )
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.name").value(productRequest.name))
    }

}
