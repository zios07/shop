package de.imedia24.shop.db.entity

import de.imedia24.shop.domain.product.ProductRequest
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    var sku: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "price", nullable = false)
    var price: BigDecimal,

    @Column(name = "stock_information", nullable = false)
    var stockInformation: Int,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    var createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    var updatedAt: ZonedDateTime
) {
    companion object {
        fun fromProductRequest(productRequest: ProductRequest) =
            ProductEntity(
                productRequest.sku,
                productRequest.name,
                productRequest.description,
                productRequest.price,
                productRequest.stockInformation,
                ZonedDateTime.now(),
                ZonedDateTime.now()
            )
    }
}
