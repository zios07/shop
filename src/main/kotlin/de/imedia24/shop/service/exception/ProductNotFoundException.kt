package de.imedia24.shop.service.exception

import java.lang.RuntimeException

class ProductNotFoundException(message: String): RuntimeException(message) {
}
