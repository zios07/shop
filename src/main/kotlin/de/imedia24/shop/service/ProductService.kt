package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.exception.ProductNotFoundException
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse> {
        return productRepository.findBySkuIn(skus);
    }

    fun createProduct(productRequest: ProductRequest): ProductResponse? {
        if (productRepository.existsById(productRequest.sku)) {
            throw IllegalArgumentException("Duplicate sku")
        }
        val productEntity = ProductEntity.fromProductRequest(productRequest)
        return productRepository.save(productEntity)?.toProductResponse()
    }

    fun updateProduct(productRequest: ProductRequest): ProductResponse? {
        if (!productRepository.existsById(productRequest.sku)) {
            throw ProductNotFoundException("Product with sku ${productRequest.sku} does not exist")
        }
        val productEntity = productRepository.findBySku(productRequest.sku)!!
        productEntity?.name = productRequest.name
        productEntity?.description = productRequest.description
        productEntity?.price = productRequest.price
        return productRepository.save(productEntity).toProductResponse()
    }
}
